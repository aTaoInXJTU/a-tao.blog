package com.atao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class EurekaServe7001Application {

    public static void main(String[] args) {
        SpringApplication.run(EurekaServe7001Application.class, args);
    }

}
