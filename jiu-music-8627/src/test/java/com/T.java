package com;

import java.util.*;
import java.util.stream.Collectors;

public class T {

    static class KthLargest {
        int[] nums;
        int k, n, ans;
        TreeMap<Integer, Integer> map;

        public KthLargest(int k, int[] nums) {
            this.k = k;
            Arrays.sort(nums);
            this.nums = nums;
            n = nums.length;
            n = 0;
            map = new TreeMap<>();
            if (nums.length >= k) {
                for (int i = nums.length - 1; i >= nums.length - k; i--) {
                    map.put(nums[i], map.getOrDefault(nums[i], 0) + 1);
                    n++;
                }
            }
        }

        public int add(int val) {
            if (n < k) {
                map.put(val, map.getOrDefault(val, 0) + 1);
                n++;
                return map.firstKey();
            }
            int f = map.firstKey();
            if (val <= f) {
                return f;
            }
            map.put(val, map.getOrDefault(val, 0) + 1);
            if (map.get(f) == 1) {
                map.remove(f);
                return map.firstKey();
            }
            map.put(f, map.get(f) - 1);
            return f;
        }
    }

    public static void main(String[] args) {
        KthLargest largest = new KthLargest(2, new int[]{0});
        largest.add(-1);
        int add = largest.add(1);
        System.out.println(add);
    }
}
