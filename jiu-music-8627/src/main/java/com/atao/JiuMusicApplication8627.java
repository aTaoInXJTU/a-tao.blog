package com.atao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableCircuitBreaker
@EnableEurekaClient
public class JiuMusicApplication8627 {

    public static void main(String[] args) {
        SpringApplication.run(JiuMusicApplication8627.class, args);
    }

}
