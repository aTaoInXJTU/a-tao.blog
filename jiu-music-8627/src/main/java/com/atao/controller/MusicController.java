package com.atao.controller;

import com.atao.domain.Result;
import com.atao.service.MusicService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/jiujiu")
public class MusicController {

    @Autowired
    public MusicService musicService;

    /**
     * 获取音乐列表
     * @param keyword 关键字
     * @return 音乐列表
     */
    @GetMapping("/getMusic")
    public Result getMusic(@RequestParam("keyword") String keyword) {
        return musicService.getMusic(keyword);
    }

    /**
     * 获取评论
     * @param id 歌曲id
     * @return 评论
     */
    @HystrixCommand(fallbackMethod = "errMusic",
            commandProperties =
                    {
                            //规定 1 秒钟以内就不报错，正常运行，超过 5 秒就报错，调用指定的方法
                            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "3000"),
                            //只能有20个并发线程来访问这个方法
                            @HystrixProperty(name="execution.isolation.strategy", value="SEMAPHORE"),
                            @HystrixProperty(name="execution.isolation.semaphore.maxConcurrentRequests", value="20"),
                            //是否开启熔断器
                            @HystrixProperty(name = "circuitBreaker.enabled", value = "true"),
                            //统计时间窗
                            @HystrixProperty(name = "metrics.rollingStats.timeInMilliseconds",value = "1000"),
                            //统计时间窗内请求次数
                            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "10"),
                            //休眠时间窗口期
                            @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
                            //在统计时间窗口期以内，请求失败率达到 60% 时进入熔断状态
                            @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "60"),
                    })
    @GetMapping("/getMusicInfo")
    public Result getMusicInfo(@RequestParam("id") String id) {
        return musicService.getMusicInfo(id);
    }

    /**
     * 获取歌曲url
     * @param id id
     */
    @HystrixCommand(fallbackMethod = "errMusic",
            commandProperties =
                    {
                            //规定 1 秒钟以内就不报错，正常运行，超过 5 秒就报错，调用指定的方法
                            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "3000"),
                            //只能有20个并发线程来访问这个方法
                            @HystrixProperty(name="execution.isolation.strategy", value="SEMAPHORE"),
                            @HystrixProperty(name="execution.isolation.semaphore.maxConcurrentRequests", value="20"),
                            //是否开启熔断器
                            @HystrixProperty(name = "circuitBreaker.enabled", value = "true"),
                            //统计时间窗
                            @HystrixProperty(name = "metrics.rollingStats.timeInMilliseconds",value = "1000"),
                            //统计时间窗内请求次数
                            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "5"),
                            //休眠时间窗口期
                            @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
                            //在统计时间窗口期以内，请求失败率达到 60% 时进入熔断状态
                            @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "60"),
                    })
    @GetMapping("/getMusicUrl")
    public Result getMusicUrl(@RequestParam("id") String id) {
        return musicService.getMusicUrl(id);
    }

    public Result errMusic(@RequestParam("id") String id) {
        return Result.fail("请求过于频繁！");
    }

    /**
     * 获取音乐记录
     */
    @GetMapping("/getMusicLogs")
    public Result getMusicLogs() {
        return musicService.getMusicLogs();
    }

    /**
     * 更新音乐记录
     */
    @GetMapping("/updateMusicLog/{id}")
    public Result updateMusicLog(@PathVariable("id") String id) {
        return musicService.updateMusicLog(id);
    }

    /**
     * 删除音乐记录
     */
    @DeleteMapping("/deleteMusicLog/{id}/{musicName}/{musicArtist}")
    public Result deleteMusicLog(@PathVariable("id") String id,
                                 @PathVariable(value = "musicName", required = false) String musicName,
                                 @PathVariable(value = "musicArtist", required = false) String musicArtist) {
        return musicService.deleteMusicLog(id, musicName, musicArtist);
    }

    /**
     * 获取下一首歌url
     * @param curId 当前歌曲id
     * @return url
     */
    @GetMapping("/getNextMusicUrl")
    public Result getNextMusicUrl(@RequestParam("curId") String curId) {
        return musicService.getNextMusicUrl(curId);
    }

}
