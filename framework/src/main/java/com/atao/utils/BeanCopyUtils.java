package com.atao.utils;

import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 对象拷贝
 */
public class BeanCopyUtils {
    private BeanCopyUtils(){}

    public static <V> V copyBean(Object source, Class<V> clazz) {
        V instance = null;
        try {
            instance = clazz.newInstance();
            BeanUtils.copyProperties(source, instance);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return instance;
    }


    public static <O, V> List<V> copyBeanList(List<O> source, Class<V> clazz) {
//        List<V> instances = new ArrayList<>();
//        for (Object o : source) {
//            try {
//                V instance = clazz.newInstance();
//                BeanUtils.copyProperties(o, instance);
//                instances.add(instance);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//
//        return instances;
        return source.stream().map(i -> copyBean(i, clazz)).collect(Collectors.toList());
    }
}
