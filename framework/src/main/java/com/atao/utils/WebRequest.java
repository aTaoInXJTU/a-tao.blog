package com.atao.utils;

import cn.hutool.core.lang.UUID;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.atao.constants.SystemConstant;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class WebRequest {
    // HTTP GET请求
    //direct null img-api 1 姓名api
    public static String sendGet(String url, Integer direct) {
        try {
//            Connection conn = Jsoup.connect(url);
//            Connection.Response demo = conn.ignoreContentType(true).method(Connection.Method.GET)
//                    .execute();
//            Document documentDemo = demo.parse();
//            return documentDemo.location();
            Connection.Response res = Jsoup.connect(url)
                    .ignoreContentType(true)
                    .method(Connection.Method.GET)
                    .execute();
            //img-api
            if (direct == null) {
                Document documentDemo = res.parse();
                return documentDemo.location();
            } else if (direct == 1) {
                String body = res.body();
                JSONObject s = JSONUtil.parseObj(body);
                return String.valueOf(s.get("xingming"));
            }
        } catch (Exception e) {
            if (direct == null) {
                return SystemConstant.DEFAULT_AVATER;
            } else if (direct == 1) {
                return UUID.randomUUID().toString().substring(0, 6);
            }
            e.printStackTrace();
        }
        return null;
    }
}
