package com.atao.utils;


import net.coobird.thumbnailator.Thumbnails;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.Objects;

/**
 * 压缩图片
 */


public class ImageUtils {
    /**
     * 压缩图片（通过降低图片质量）
     * 压缩图片,通过压缩图片质量，保持原图大小
     *  quality 0.5f
     *       图片质量（0-1）
     * @return byte[]
     *      压缩后的图片（jpg）
     */
    public static InputStream compressPicByQuality(InputStream img) {
        try {
//            File file = transferToFile(img);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            //scale(比例),outputQuality(质量)
            Thumbnails.of(img).scale(1f).outputQuality(0.25f).toOutputStream(out);
            return streamTran(out);
        } catch (IOException e) {
            System.out.println("文件压缩错误");
        }
        return img;
    }

    /**
     * MultipartFile 转换为 File 文件
     *
     * @param multipartFile
     * @return
     */
    public static File transferToFile(MultipartFile multipartFile) {
        //选择用缓冲区来实现这个转换即使用java 创建的临时文件 使用 MultipartFile.transferto()方法 。
        File file = null;
        try {
            String originalFilename = multipartFile.getOriginalFilename();
            String[] filename = Objects.requireNonNull(originalFilename).split("\\.");
            file = File.createTempFile(filename[0], filename[1]);    //注意下面的 特别注意！！！
            multipartFile.transferTo(file);
            file.deleteOnExit();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    /**
     * ByteArrayOutputStream 转 inputStream
     * @param in
     * @return
     */
    public static InputStream streamTran(ByteArrayOutputStream in) {
        return new ByteArrayInputStream(in.toByteArray());
    }

}
