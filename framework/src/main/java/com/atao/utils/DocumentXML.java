package com.atao.utils;

import com.atao.domain.vo.LocalSearchVo;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.List;

/**
 * Document生成XML
 * @author ouyangjun
 */
public class DocumentXML {

    /**
     * 生成local-search.xml 文件
     * @param file 临时search文件
     * @param list LocalSearchVo
     */
    public static void createDocument(File file, List<LocalSearchVo> list) {
        try {
            // 初始化一个XML解析工厂
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

            // 创建一个DocumentBuilder实例
            DocumentBuilder builder = factory.newDocumentBuilder();

            // 构建一个Document实例
            Document doc = builder.newDocument();
            doc.setXmlStandalone(true);
            // standalone用来表示该文件是否呼叫其它外部的文件。若值是 ”yes” 表示没有呼叫外部文件

            // 创建一个根节点
            // 说明: doc.createElement("元素名")、element.setAttribute("属性名","属性值")、element.setTextContent("标签间内容")
            Element element = doc.createElement("search");
//            element.setAttribute("attr", "root");


            for (LocalSearchVo o : list) {
                // 创建根节点第n个子节点
                Element elementChildOne = doc.createElement("entry");
                element.appendChild(elementChildOne);
                createLocalSearchChildEl(doc, elementChildOne, o);
            }

            // 添加根节点
            doc.appendChild(element);

            // 把构造的XML结构，写入到具体的文件中
            TransformerFactory formerFactory=TransformerFactory.newInstance();
            Transformer transformer=formerFactory.newTransformer();
            // 换行
            transformer.setOutputProperty(OutputKeys.INDENT, "YES");
            // 文档字符编码
            transformer.setOutputProperty(OutputKeys.ENCODING, "utf-8");

            // 可随意指定文件的后缀,效果一样,但xml比较好解析,比如: E:\\person.txt等
            transformer.transform(new DOMSource(doc),new StreamResult(file));

            System.out.println("XML CreateDocument success!");
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

    private static void createLocalSearchChildEl(Document doc, Element element, LocalSearchVo vo) {
        // 第一个子节点的第一个子节点
        Element child1 = doc.createElement("title");
        child1.setTextContent(vo.getTitle());

        Element child2 = doc.createElement("link");
        child2.setAttribute("href", "detail.html?id="+vo.getLink());

        Element child3 = doc.createElement("url");
        child3.setTextContent("detail.html?id="+vo.getUrl());

        //<![CDATA[" 开始，由 "]]>
        Element child4 = doc.createElement("content");
        child4.setAttribute("type", "html");
//        String preCDATA = "<![CDATA[";
//        String sufCDATA = "]]>";
        CDATASection cdataSection = doc.createCDATASection("content");
        cdataSection.setTextContent(vo.getContent());
        child4.appendChild(cdataSection);


        Element child5 = doc.createElement("categories");
        for (String category : vo.getCategories()) {
            Element child51 = doc.createElement("category");
            child51.setTextContent(category);
            child5.appendChild(child51);
        }

        Element child6 = doc.createElement("tags");
        for (String tag : vo.getTags()) {
            Element child61 = doc.createElement("tag");
            child61.setTextContent(tag);
            child6.appendChild(child61);
        }

        element.appendChild(child1);
        element.appendChild(child2);
        element.appendChild(child3);
        element.appendChild(child4);
        element.appendChild(child5);
        element.appendChild(child6);

    }
}