package com.atao.utils;

import java.text.DecimalFormat;
import java.util.Date;

public class TimeDateUtils {

    /**
     * 获取时间差方法,返回时分秒 HH:mm:ss
     *
     * @param currentTime
     * @param firstTime
     * @return
     */
    public static String getTimeInterval(Date currentTime, Date firstTime) {
        DecimalFormat decimalFormat = new DecimalFormat("00");
        long diff = currentTime.getTime() - firstTime.getTime();//得到的差值
        long hours = diff / (1000 * 60 * 60); //获取时
        long minutes = (diff - hours * (1000 * 60 * 60)) / (1000 * 60);  //获取分钟
        long s = (diff / 1000 - hours * 60 * 60 - minutes * 60);//获取秒
        if (hours != 0L) {
            if (hours >= 24L) {
                return hours / 24 + "天前";
            }
            return hours + "小时前";
        } else if (minutes != 0L) {
            return minutes + "分钟前";
        } else if (s != 0L) {
            if (s <= 20L) {
                return "刚刚";
            } else {
                return s + "秒前";
            }
        }
        return "" + decimalFormat.format(hours) + ":" + decimalFormat.format(minutes) + ":" + decimalFormat.format(s);
    }

}
