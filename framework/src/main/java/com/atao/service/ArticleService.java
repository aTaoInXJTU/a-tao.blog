package com.atao.service;

import com.atao.domain.Result;
import com.atao.domain.entity.Article;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpServletResponse;
import java.util.List;


/**
 * (Article)表服务接口
 *
 * @author makejava
 * @since 2023-02-09 16:07:05
 */
public interface ArticleService extends IService<Article> {
    //Result hotArticleList();

    Result articleList(Integer pageNum, Integer pageSize, Long categoryId);

    //Result articleList(Integer pageNum, Integer pageSize, Long categoryId);

    Result getArticleDetail(Long id);

    Result deleteArticle(List<Long> ids);

    Result addArticle(Article article);

    Result articleBackstageHomePage(Integer pageNum, Integer pageSize);

    Result articleSearchByTag(Long tagId);

    void exportExcel(HttpServletResponse response);

    Result getBeforeAfterArticles(Long id);

    Result updateReaction(Long articleId, List<Long> cnt);

    Result getCategoryArticle();

    Result getTagArticle();

    Result listByTagId(Long id);

    Result listByCategoryId(Long id);

    void updateLocalSearchXML();
}
