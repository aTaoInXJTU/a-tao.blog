package com.atao.service;

import com.atao.domain.Result;

import java.util.List;

public interface MusicService {
    Result getMusic(String keyword);

    Result getMusicInfo(String id);

    Result getMusicUrl(String id);

    Result getMusicLogs();

    Result updateMusicLog(String id);

    Result deleteMusicLog(String id, String musicName, String musicArtist);

    Result getNextMusicUrl(String curId);
}
