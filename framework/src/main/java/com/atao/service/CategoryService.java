package com.atao.service;

import com.atao.domain.Result;
import com.atao.domain.entity.Category;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpServletResponse;


/**
 * 分类表(Category)表服务接口
 *
 * @author makejava
 * @since 2023-02-09 17:06:01
 */
public interface CategoryService extends IService<Category> {
    Result getCategoryName(Long categoryId);

    Result getAllCategory();

    Result deleteCategory(Long id, HttpServletResponse response);

    Result categoryEdit(Category category);
}
