package com.atao.service;

import com.atao.domain.entity.ArticleTag;
import com.baomidou.mybatisplus.extension.service.IService;


/**
 * 文章标签关联表(ArticleTag)表服务接口
 *
 * @author makejava
 * @since 2023-02-09 19:46:10
 */
public interface ArticleTagService extends IService<ArticleTag> {

}
