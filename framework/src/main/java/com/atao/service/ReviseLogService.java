package com.atao.service;

import com.atao.domain.Result;
import com.atao.domain.entity.ReviseLog;
import com.baomidou.mybatisplus.extension.service.IService;


/**
 * (ReviseLog)表服务接口
 *
 * @author makejava
 * @since 2023-03-05 16:06:14
 */
public interface ReviseLogService extends IService<ReviseLog> {

    Result reviseLogAdd(ReviseLog reviseLog);

    Result getAllReviseLog();
}
