package com.atao.service.impl;

import com.atao.domain.Result;
import com.atao.domain.entity.Tag;
import com.atao.mapper.TagMapper;
import com.atao.service.TagService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 标签(Tag)表服务实现类
 *
 * @author makejava
 * @since 2023-02-09 19:42:30
 */
@Service("tagService")
public class TagServiceImpl extends ServiceImpl<TagMapper, Tag> implements TagService {

    @Override
    public Result getAllTag() {
        return Result.ok(list());
    }
}
