package com.atao.service.impl;

import com.atao.domain.entity.AuthorLocation;
import com.atao.mapper.AuthorLocationMapper;
import com.atao.service.AuthorLocationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * (AuthorLocation)表服务实现类
 *
 * @author makejava
 * @since 2023-03-05 16:07:13
 */
@Service("authorLocationService")
public class AuthorLocationServiceImpl extends ServiceImpl<AuthorLocationMapper, AuthorLocation> implements AuthorLocationService {

}
