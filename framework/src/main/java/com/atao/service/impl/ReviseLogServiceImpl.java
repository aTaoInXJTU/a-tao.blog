package com.atao.service.impl;

import cn.hutool.json.JSONUtil;
import com.atao.constants.SystemConstant;
import com.atao.domain.Result;
import com.atao.domain.entity.Article;
import com.atao.domain.entity.Author;
import com.atao.domain.entity.ReviseLog;
import com.atao.domain.vo.ReviseLogVo;
import com.atao.mapper.ReviseLogMapper;
import com.atao.service.ArticleService;
import com.atao.service.AuthorService;
import com.atao.service.ReviseLogService;
import com.atao.utils.BeanCopyUtils;
import com.atao.utils.TimeDateUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * (ReviseLog)表服务实现类
 *
 * @author makejava
 * @since 2023-03-05 16:06:14
 */
@Service("reviseLogService")
public class ReviseLogServiceImpl extends ServiceImpl<ReviseLogMapper, ReviseLog> implements ReviseLogService {

    @Autowired
    private AuthorService authorService;
    @Autowired
    private ArticleService articleService;
    @Autowired
    private StringRedisTemplate redisTemplate;

    @Override
    public Result reviseLogAdd(ReviseLog reviseLog) {
        save(reviseLog);
        redisTemplate.delete(SystemConstant.ARTICLE_REVERSE_LOG);
        return Result.ok();
    }

    @Override
    public Result getAllReviseLog() {
        List<ReviseLogVo> logs = null;
        String redisInfo = redisTemplate.opsForValue().get(SystemConstant.ARTICLE_REVERSE_LOG);
        if (redisInfo != null) {
            logs = JSONUtil.toList(redisInfo, ReviseLogVo.class);
            return Result.ok(logs);
        }
        logs = BeanCopyUtils.copyBeanList(list(), ReviseLogVo.class);
        for (ReviseLogVo reviseLogVo : logs) {
            String createBy = reviseLogVo.getCreateBy();
            LambdaQueryWrapper<Author> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(Author::getName, createBy);
            Author author = authorService.getOne(wrapper);
            reviseLogVo.setImg(author.getImg());
            //添加
            if ("0".equals(reviseLogVo.getOperate())) {
                if ("文章".equals(reviseLogVo.getDirectionTable())) {
                    reviseLogVo.setLogInfo("发布了名为《"+ reviseLogVo.getValueName() +"》的文章");
                    LambdaQueryWrapper<Article> wrapper1 = new LambdaQueryWrapper<>();
                    wrapper1.eq(reviseLogVo.getValueName() != null, Article::getArticleTitle, reviseLogVo.getValueName());
                    List<Article> articleList = articleService.list(wrapper1);
                    //之前添加的文章已经被删除
                    if (articleList.size() != 0) {
                        reviseLogVo.setObjInfo(articleList.get(0));
                    }
                } else {
                    reviseLogVo.setLogInfo("添加了 "+ reviseLogVo.getDirectionTable() +"："+ reviseLogVo.getValueName());
                }
            } else if ("1".equals(reviseLogVo.getOperate())) {
                if ("文章".equals(reviseLogVo.getDirectionTable())) {
                    reviseLogVo.setLogInfo("删除了名为《"+ reviseLogVo.getValueName() +"》的文章");
                } else {
                    reviseLogVo.setLogInfo("删除了 "+ reviseLogVo.getDirectionTable() +"："+ reviseLogVo.getValueName());
                }
            } else {
                if ("文章".equals(reviseLogVo.getDirectionTable())) {
                    reviseLogVo.setLogInfo("修改了名为《"+ reviseLogVo.getValueName() +"》的文章");
                    LambdaQueryWrapper<Article> wrapper1 = new LambdaQueryWrapper<>();
                    wrapper1.eq(reviseLogVo.getValueName() != null, Article::getArticleTitle, reviseLogVo.getValueName());
                    reviseLogVo.setObjInfo(articleService.getOne(wrapper1));
                } else {
                    reviseLogVo.setLogInfo("修改了 "+ reviseLogVo.getDirectionTable() +"："+ reviseLogVo.getValueName());
                }
            }
            Date createTime = reviseLogVo.getCreateTime();
            String timeInterval = TimeDateUtils.getTimeInterval(new Date(), createTime);
            reviseLogVo.setDateInfo(timeInterval);
        }
        Collections.reverse(logs);
        redisTemplate.opsForValue().set(SystemConstant.ARTICLE_REVERSE_LOG, JSONUtil.toJsonStr(logs));
        return Result.ok(logs);
    }
}
