package com.atao.service.impl;

import cn.hutool.json.JSONUtil;
import com.atao.constants.SystemConstant;
import com.atao.domain.Result;
import com.atao.domain.entity.Article;
import com.atao.domain.entity.Category;
import com.atao.mapper.CategoryMapper;
import com.atao.service.ArticleService;
import com.atao.service.AuthorService;
import com.atao.service.CategoryService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Set;

/**
 * 分类表(Category)表服务实现类
 *
 * @author makejava
 * @since 2023-02-09 17:06:01
 */
@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService {

    @Autowired
    private ArticleService articleService;
    @Autowired
    private AuthorService authorService;
    @Autowired
    private StringRedisTemplate redisTemplate;

    @Override
    public Result getCategoryName(Long categoryId) {
        Category category = null;
        String redisKey = SystemConstant.ARTICLE_CATEGORY + categoryId;
        String redisRes = redisTemplate.opsForValue().get(redisKey);
        if (redisRes != null) {
            category = JSONUtil.toBean(redisRes, Category.class);
            return Result.ok(category);
        }
        category = getById(categoryId);
        redisTemplate.opsForValue().set(redisKey, JSONUtil.toJsonStr(category));
        return Result.ok(category);
    }

    @Override
    public Result getAllCategory() {
        List<Category> categories = null;
        String redisKey = SystemConstant.ARTICLE_CATEGORY + "all";
        String redisRes = redisTemplate.opsForValue().get(redisKey);
        if (redisRes != null) {
            categories = JSONUtil.toList(redisRes, Category.class);
            return Result.ok(categories);
        }
        categories = list();
        redisTemplate.opsForValue().set(redisKey, JSONUtil.toJsonStr(categories));
        return Result.ok(categories);
    }

    @Override
    public Result deleteCategory(Long id, HttpServletResponse response) {
        LambdaQueryWrapper<Article> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Article::getArticleCategoryId, id);
        int count = articleService.count(wrapper);
        if (count != 0) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            return Result.fail("有文章关联此目录！删除失败！");
        }
        this.removeById(id);
        //删除redis
        Set<String> keys = redisTemplate.keys(SystemConstant.ARTICLE_CATEGORY + "*");
        redisTemplate.delete(keys);
        return Result.ok();
    }

    @Override
    public Result categoryEdit(Category category) {
        saveOrUpdate(category);
        Set<String> keys = redisTemplate.keys(SystemConstant.ARTICLE_CATEGORY + "*");
        redisTemplate.delete(keys);
        return Result.ok();
    }
}
