package com.atao.service;

import com.atao.domain.Result;
import com.atao.domain.entity.Tag;
import com.baomidou.mybatisplus.extension.service.IService;


/**
 * 标签(Tag)表服务接口
 *
 * @author makejava
 * @since 2023-02-09 19:42:30
 */
public interface TagService extends IService<Tag> {

    Result getAllTag();
}
