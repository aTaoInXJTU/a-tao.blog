package com.atao.service;

import com.atao.domain.Result;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

public interface UploadService {
    Result uploadImg(MultipartFile img);
    Result uploadFile(File file);
    void deleteOss(String filePath);
}
