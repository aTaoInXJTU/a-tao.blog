package com.atao.service;

import com.atao.domain.Result;
import com.atao.domain.entity.Author;
import com.baomidou.mybatisplus.extension.service.IService;


/**
 * (Author)表服务接口
 *
 * @author makejava
 * @since 2023-03-05 16:03:24
 */
public interface AuthorService extends IService<Author> {

    Result authorList();

    Result getCurAuthor(String userInfo);

    Result addAuthor(String location, String author);
}
