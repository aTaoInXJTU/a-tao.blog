package com.atao.service;

import com.atao.domain.Result;
import com.atao.domain.entity.ArticleComment;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;


/**
 * (ArticleComment)表服务接口
 *
 * @author makejava
 * @since 2023-03-23 20:14:39
 */
public interface ArticleCommentService extends IService<ArticleComment> {

    Result getArticleCommentByArticleId(Long id);

    Result addArticleComment(ArticleComment articleComment);

    Result updateReaction(Integer id,  Integer commentId, Integer like, List<Integer> cnt);
}
