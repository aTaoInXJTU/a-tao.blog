package com.atao.service;

import com.atao.domain.entity.AuthorLocation;
import com.baomidou.mybatisplus.extension.service.IService;


/**
 * (AuthorLocation)表服务接口
 *
 * @author makejava
 * @since 2023-03-05 16:07:12
 */
public interface AuthorLocationService extends IService<AuthorLocation> {

}
