package com.atao.mapper;

import com.atao.domain.entity.Tag;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
/**
 * 标签(Tag)表数据库访问层
 *
 * @author makejava
 * @since 2023-02-09 19:42:16
 */
public interface TagMapper extends BaseMapper<Tag> {

}
