package com.atao.mapper;

import com.atao.domain.entity.AuthorLocation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


@Mapper
/**
 * (AuthorLocation)表数据库访问层
 *
 * @author makejava
 * @since 2023-03-05 16:07:12
 */
public interface AuthorLocationMapper extends BaseMapper<AuthorLocation> {

}
