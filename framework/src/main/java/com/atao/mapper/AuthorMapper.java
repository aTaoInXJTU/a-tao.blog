package com.atao.mapper;

import com.atao.domain.entity.Author;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


@Mapper
/**
 * (Author)表数据库访问层
 *
 * @author makejava
 * @since 2023-03-05 16:03:24
 */
public interface AuthorMapper extends BaseMapper<Author> {

}
