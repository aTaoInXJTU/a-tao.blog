package com.atao.mapper;

import com.atao.domain.entity.Category;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


/**
 * 分类表(Category)表数据库访问层
 *
 * @author makejava
 * @since 2023-02-09 17:05:59
 */
@Mapper
public interface CategoryMapper extends BaseMapper<Category> {

}
