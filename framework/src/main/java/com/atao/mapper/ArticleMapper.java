package com.atao.mapper;

import com.atao.domain.entity.Article;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


/**
 * (Article)表数据库访问层
 *
 * @author makejava
 * @since 2023-02-09 16:07:02
 */
@Mapper
public interface ArticleMapper extends BaseMapper<Article> {

}
