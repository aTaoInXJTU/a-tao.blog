package com.atao.mapper;

import com.atao.domain.entity.ArticleTag;
import com.atao.domain.vo.TagVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

@Mapper
/**
 * 文章标签关联表(ArticleTag)表数据库访问层
 *
 * @author makejava
 * @since 2023-02-09 19:46:10
 */
public interface ArticleTagMapper extends BaseMapper<ArticleTag> {
    @Select("select tag_id, count(*) as cnt from article_tag group by tag_id")
    List<TagVo> getTagArticleBysql();
}
