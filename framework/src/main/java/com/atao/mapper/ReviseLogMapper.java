package com.atao.mapper;

import com.atao.domain.entity.ReviseLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


@Mapper
/**
 * (ReviseLog)表数据库访问层
 *
 * @author makejava
 * @since 2023-03-05 16:06:14
 */
public interface ReviseLogMapper extends BaseMapper<ReviseLog> {

}
