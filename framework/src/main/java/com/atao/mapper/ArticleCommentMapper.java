package com.atao.mapper;

import com.atao.domain.entity.ArticleComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


@Mapper
/**
 * (ArticleComment)表数据库访问层
 *
 * @author makejava
 * @since 2023-03-23 20:14:38
 */
public interface ArticleCommentMapper extends BaseMapper<ArticleComment> {

}
