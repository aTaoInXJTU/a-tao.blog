package com.atao.constants;

public class SystemConstant {
    /**
     * 页码
     */
    public static final int DEFAULT_PAGE_SIZE = 5;
    public static final String DEFAULT_PAGE_SIZE_STR = "5";

    /**
     * redis 过期时间
     */
    public static final Integer CACHE_TTL = 720;
    public static final Integer CACHE_NULL_TTL = 1;

    /**
     * 音乐
     */
    public static final String REQ_ID = "music:reqid:";
    public static final String MUSIC_SIMPLY_INFO = "music:simply:info:";
    public static final String MUSIC_SPECIFIC_INFO = "music:specific:info:";
    public static final String MUSIC_URL = "music:url:";
    public static final String MUSIC_LOG = "music:log:";
    /**
     * oss图片上传路径
     */
    public static final String BLOG_IMG_PATH = "atao/";
    /**
     * oss url
     */
    public static final String BLOG_OSS_URL = "https://cdn.qiniu.a-tao.top/";

    /**
     * 用户默认描述
     */
    public static final String AUTHOR_DEFAULT_DESCRIPTION = "这个用户很懒，没有留下任何信息！";

    /**
     * 随机头像api
     */
    public static final String AUTHOR_IMG_API = "https://pic.imgdb.cn/api/avatar";
    /**
     * 默认头像
     */
    public static final String DEFAULT_AVATER = "https://cdn.qiniu.a-tao.top/atao/default_avater.png";
    /**
     * 游客姓名api
     */
    public static final String VISITOR_RANDOM_NAME_API = "https://v.api.aa1.cn/api/api-xingming/index.php";

    /**
     * 访问首页后缀 ip + port + suffix
     */
    public static final String WEB_INDEX_SUFFIX = "/templates/index.html";
    /**
     * 访问首页后缀 suffix
     */
    public static final String WEB_INDEX_HTML = "/index.html";
    /**
     * 访问域名
     */
    public static final String WEB_DOMAIN_NAME = "https://www.a-tao.top/";

    /**
     * 访问首页后缀 ip + port + suffix
     */
    public static final String WEB_NON_INDEX_SUFFIX = "/templates/";

    /**
     * 后台游客id
     */
    public static final Integer VISITOR_ID = 3;

    /**
     * 文章评论默认反应
     */
    public static final String ARTICLE_COMMENT_DEFAULT_EMOJIS = "0 0 0 0 0 0 0 0 0 0 0 0";

    /**
     * 文章
     */
    public static final String ARTICLE_LIST = "article:list:";
    public static final String ARTICLE_LIST_TAG = "article:list:tag:";
    public static final String ARTICLE_LIST_BY_TAG = "article:list:simp:tag:";
    public static final String ARTICLE_LIST_BY_CATEGORY = "article:list:simp:category:";
    public static final String ARTICLE_CNT = "article:cnt";
    public static final String ARTICLE_DETAIL = "article:detail:";
    public static final String ARTICLE_CATEGORY = "article:category:";
    public static final String ARTICLE_COMMENT = "article:comment:";
    public static final String ARTICLE_USER = "article:user:";
    public static final String ARTICLE_REVERSE_LOG = "article:revise:log";
}
