package com.atao.domain.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class ArticleExcelDto {
    @ExcelProperty("文章编号")
    @ColumnWidth(20)
    private Long articleId;

    @ExcelProperty("文章标题")
    @ColumnWidth(20)
    private String articleTitle;

    @ExcelProperty("文章摘要")
    @ColumnWidth(20)
    private String articleAbstract;

    @ExcelProperty("文章内容")
    @ColumnWidth(20)
    private String articleContent;

    @ExcelProperty("文章目录")
    @ColumnWidth(20)
    private String categoryName;

    @ExcelProperty("标签")
    @ColumnWidth(20)
    private String tagName;

    @ExcelProperty("作者")
    @ColumnWidth(20)
    private String createBy;

    @ExcelProperty("创建时间")
    @ColumnWidth(20)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date createTime;

    @ExcelProperty("更新人")
    @ColumnWidth(20)
    private String updateBy;

    @ExcelProperty("更新时间")
    @ColumnWidth(20)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date updateTime;
}
