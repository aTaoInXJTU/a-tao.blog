package com.atao.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 获取下一首歌url的vo
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MusicUrlVo {
    private String musicId;
    private String musicUrl;
}
