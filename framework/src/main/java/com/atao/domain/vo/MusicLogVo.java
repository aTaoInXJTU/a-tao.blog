package com.atao.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MusicLogVo {
    private String musicId;
    private String musicName;
    private String musicArtist;
    private Integer freq;
}
