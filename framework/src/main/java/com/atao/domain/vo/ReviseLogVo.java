package com.atao.domain.vo;

import com.atao.domain.entity.ReviseLog;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReviseLogVo extends ReviseLog {
    private String logInfo;
    private String img;
    private String dateInfo;
    private Object objInfo;
}
