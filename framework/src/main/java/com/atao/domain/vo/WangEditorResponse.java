package com.atao.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


//1、先建一个返回信息的封装类
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WangEditorResponse {
    private Integer errno;
    private WangEditorImgResponse data;
}
