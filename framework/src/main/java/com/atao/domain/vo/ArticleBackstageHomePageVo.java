package com.atao.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ArticleBackstageHomePageVo {
    private Long articleId;
    private String articleImage;
    private String articleTitle;
}
