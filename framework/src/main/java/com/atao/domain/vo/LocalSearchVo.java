package com.atao.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LocalSearchVo {
    private String title;
    private String link;
    private String url;
    private String content;
    private List<String> categories;
    private List<String> tags;
}
