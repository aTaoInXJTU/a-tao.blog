package com.atao.domain.entity;

import java.util.Date;

import java.io.Serializable;
import java.util.List;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
/**
 * (ArticleComment)表实体类
 *
 * @author makejava
 * @since 2023-03-23 20:14:38
 */
@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("article_comment")
public class ArticleComment  {
    @TableId
    private Long id;

    
    private Long articleId;
    //内容
    private String content;
    //点赞
    private Long likes;
    //表情符号数量，以空格分割
    private String emojis;

    @TableField(exist = false)
    private List<Long> emojisCnt;
    //父评论
    private Integer pid;

    @TableField(exist = false)
    private List<ArticleComment> subComments;

    private String createBy;

    private Date createTime;
    //头像
    private String createImg;
    
    private Integer createById;
    
    private Integer delFlag;



}
