package com.atao.domain.entity;

import java.util.Date;

import java.io.Serializable;
import java.util.List;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * (Article)表实体类
 *
 * @author makejava
 * @since 2023-02-09 16:07:04
 */
@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("article")
public class Article  {
    //主键id@TableId
    @TableId(type = IdType.AUTO)
    private Long articleId;

    private String articleTitle;
    
    private String articleAbstract;
    
    private String articleImage;
    
    private String articleContent;
    
    private String articleStatus;
    
    private Long articleCategoryId;
    //所属分类id
    @TableField(exist = false)
    private String categoryName;

    //表情符号数量，以空格分割
    private String articleReaction;

    @TableField(exist = false)
    private List<Long> articleReactionCnt;

    @TableField(exist = false)
    private List<Long> articleTagId;
    //所属标签id
    @TableField(exist = false)
    private List<String> tagName;

    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
    
    private Integer delFlag;

}
