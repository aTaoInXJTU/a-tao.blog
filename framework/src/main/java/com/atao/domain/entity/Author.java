package com.atao.domain.entity;


import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
/**
 * (Author)表实体类
 *
 * @author makejava
 * @since 2023-03-05 16:03:24
 */
@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("author")
public class Author  {
    @TableId
    private Integer id;

    
    private String name;
    
    private String img;
    
    private String description;



}
