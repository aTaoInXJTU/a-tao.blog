package com.atao.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MusicComment {
    private String umsg;
    private String ulike;
    private String uname;
    private String upic;
    private String time;
}
