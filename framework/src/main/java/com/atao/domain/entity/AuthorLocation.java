package com.atao.domain.entity;


import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
/**
 * (AuthorLocation)表实体类
 *
 * @author makejava
 * @since 2023-03-05 16:07:12
 */
@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("author_location")
public class AuthorLocation  {
    @TableId
    private Long id;

    
    private String location;
    
    private Integer author;



}
