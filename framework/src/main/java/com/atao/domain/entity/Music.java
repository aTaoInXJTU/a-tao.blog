package com.atao.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Music {
    private String musicId;
    private String musicName;
    private String musicArtist;
    private Integer musicFreq;
    private Date musicLatest;
    private MusicComment[] musicComments;
    private String[][] timeStamp;
}
