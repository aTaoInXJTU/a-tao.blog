package com.atao.domain.entity;

import java.util.Date;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
/**
 * (ReviseLog)表实体类
 *
 * @author makejava
 * @since 2023-03-05 16:06:14
 */
@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("revise_log")
public class ReviseLog  {
    @TableId
    private Long id;
    private String operate; //操作 0 增加 1 删除 2 修改
    private String valueId; //修改和删除的id:名称 添加的名称
    private String valueName; //修改和删除的id:名称 添加的名称


    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    private String directionTable; //哪张表 作者 目录 文章


}
