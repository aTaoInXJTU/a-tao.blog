package com.atao.controller;

import cn.hutool.json.JSONUtil;
import com.atao.domain.Result;
import com.atao.domain.vo.WangEditorImgResponse;
import com.atao.domain.vo.WangEditorResponse;
import com.atao.service.UploadService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

@RestController
@RequestMapping("/backstage/uploadImage")
public class UploadImageController {

    @Resource
    private UploadService uploadService;

    @PostMapping
    public String uploadImage(@RequestParam("imgFile") MultipartFile imgFile) {
        Result result = uploadService.uploadImg(imgFile);
        String value = result.getData().toString();
        String jsonString = JSONUtil.toJsonStr(new WangEditorResponse(0, new WangEditorImgResponse(value, "", value)));
        System.out.println(jsonString);
        return jsonString;
    }
}
