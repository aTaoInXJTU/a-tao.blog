package com.atao.controller;

import com.atao.domain.Result;
import com.atao.domain.entity.ReviseLog;
import com.atao.service.ReviseLogService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/backstage/reviseLog")
public class ReviseLogController {
    @Resource
    private ReviseLogService reviseLogService;

    @GetMapping("/list")
    public Result reviseLogList() {
        return reviseLogService.getAllReviseLog();
    }

    @PostMapping
    public Result reviseLogAdd(@RequestBody ReviseLog reviseLog) {
        return reviseLogService.reviseLogAdd(reviseLog);
    }
}
