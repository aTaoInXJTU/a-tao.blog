package com.atao.controller;

import com.atao.constants.SystemConstant;
import com.atao.domain.Result;
import com.atao.domain.entity.Article;
import com.atao.service.ArticleService;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/backstage/article")
public class ArticleController {

    @Resource
    private ArticleService articleService;

    @GetMapping("/list")
    public Result articleList(
            @RequestParam(defaultValue = "0")Integer pageNum,
            @RequestParam(defaultValue = SystemConstant.DEFAULT_PAGE_SIZE_STR) Integer pageSize,
            Long categoryId) {
        return articleService.articleList(pageNum, pageSize, categoryId);
    }

    /**
     * 热门搜索-->标签
     * @param tagId 标签id
     */
    @GetMapping("/searchByTag")
    public Result articleSearchByTag(@RequestParam(value = "tagId") Long tagId) {
        return articleService.articleSearchByTag(tagId);
    }

    /**
     * 后台图片展示
     * @return ArticleBackstageHomePageVo
     */
    @GetMapping("/backstageHomePage")
    public Result backstageHomePageList(
            @RequestParam(defaultValue = "0")Integer pageNum,
            @RequestParam(defaultValue = SystemConstant.DEFAULT_PAGE_SIZE_STR) Integer pageSize) {
        return articleService.articleBackstageHomePage(pageNum, pageSize);
    }

    @DeleteMapping("/{ids}")
    public Result articleDel(@PathVariable List<Long> ids) {
        return articleService.deleteArticle(ids);
    }

    @GetMapping("/{id}")
    public Result articleOne(@PathVariable Long id) {
        return articleService.getArticleDetail(id);
    }

    @PostMapping
    public Result updateArticle(@RequestBody Article article) {
        return articleService.addArticle(article);
    }

    @PostMapping("/img")
    public Result updateImg(@RequestBody Map map) {
        System.out.println(map);
        return null;
    }

    @PostMapping("/exportExcel")
    public void exportExcel(HttpServletResponse response) {
        articleService.exportExcel(response);
    }
}
