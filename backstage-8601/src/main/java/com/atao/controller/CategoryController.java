package com.atao.controller;

import com.atao.domain.Result;
import com.atao.domain.entity.Category;
import com.atao.service.CategoryService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/backstage/category")
public class CategoryController {
    @Resource
    private CategoryService categoryService;

    @GetMapping("/list")
    public Result categoryList() {
        return categoryService.getAllCategory();
    }

    @DeleteMapping("/{id}")
    public Result categoryDel(@PathVariable Long id, HttpServletResponse response) {
        return categoryService.deleteCategory(id, response);
    }

    @PostMapping
    public Result categoryEdit(@RequestBody Category category) {
        return categoryService.categoryEdit(category);
    }
}
