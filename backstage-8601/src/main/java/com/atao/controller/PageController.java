package com.atao.controller;


import com.atao.constants.SystemConstant;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/backstage/page")
public class PageController {

    @Resource
    private DiscoveryClient discoveryClient;

    /**
     * 通过 eureka 服务注册 和 发现 跳转
     * @param response 前台重定向
     * @param path Application-name
     */
    @RequestMapping("/{path}")
    public void page(HttpServletResponse response, @PathVariable String path,
                     @RequestParam(value = "id", required = false) Long id,
                     @RequestParam(value = "detailPath", required = false) String detailPath) throws IOException {
        List<ServiceInstance> instances = discoveryClient.getInstances(path);
        if ("BACKSTAGE-SERVICE".equals(path)) {
            response.sendRedirect(SystemConstant.WEB_DOMAIN_NAME + "back" + SystemConstant.WEB_INDEX_HTML);
        } else if ("JIU-MUSIC".equals(path)) {
            response.sendRedirect(SystemConstant.WEB_DOMAIN_NAME + "music" + SystemConstant.WEB_INDEX_HTML);
        } else {
            if (detailPath == null) {
                response.sendRedirect(SystemConstant.WEB_DOMAIN_NAME);
                return;
            }
            if (id == null) {
                response.sendRedirect(SystemConstant.WEB_DOMAIN_NAME + detailPath + ".html");
                return;
            }
            response.sendRedirect(SystemConstant.WEB_DOMAIN_NAME + detailPath + ".html?id=" + id);
        }
    }
}
