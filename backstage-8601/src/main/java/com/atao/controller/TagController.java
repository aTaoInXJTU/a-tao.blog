package com.atao.controller;

import com.atao.domain.Result;
import com.atao.service.TagService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/backstage/tag")
public class TagController {
    @Resource
    private TagService tagService;

    @GetMapping("/list")
    public Result categoryList() {
        return tagService.getAllTag();
    }
}
