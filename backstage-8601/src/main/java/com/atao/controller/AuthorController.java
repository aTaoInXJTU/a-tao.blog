package com.atao.controller;

import com.atao.domain.Result;
import com.atao.service.AuthorService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

@RestController
@RequestMapping("/backstage/author")
public class AuthorController {
    @Resource
    private AuthorService authorService;

    @GetMapping("/list")
    public Result authorList() {
        return authorService.authorList();
    }

    @GetMapping("/{info}/static_str")
    public Result getCurAuthor(@PathVariable("info") String userInfo) {
        return authorService.getCurAuthor(userInfo);
    }

    @PostMapping
    public Result addAuthor(@RequestBody Map<String, String> map) {
        return authorService.addAuthor(map.get("location"), map.get("author"));
    }
}
