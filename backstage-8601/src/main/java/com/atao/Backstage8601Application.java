package com.atao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class Backstage8601Application {

    public static void main(String[] args) {
        SpringApplication.run(Backstage8601Application.class, args);
    }
}
