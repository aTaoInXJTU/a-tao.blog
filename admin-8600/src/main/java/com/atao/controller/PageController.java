package com.atao.controller;


import com.atao.constants.SystemConstant;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping("/content/page")
public class PageController {

    @Resource
    private DiscoveryClient discoveryClient;

    /**
     * 通过 eureka 服务注册 和 发现 跳转
     * @param response 前台重定向
     * @param path Application-name
     */
    @RequestMapping("/{path}")
    public void page(HttpServletResponse response, @PathVariable String path) throws IOException {
//        List<ServiceInstance> instances = discoveryClient.getInstances(path);
//        response.sendRedirect(instances.get(0).getUri().toString() +
//                SystemConstant.WEB_INDEX_SUFFIX);
        if ("BACKSTAGE-SERVICE".equals(path)) {
            response.sendRedirect(SystemConstant.WEB_DOMAIN_NAME + "back" + SystemConstant.WEB_INDEX_HTML);
        } else if ("JIU-MUSIC".equals(path)) {
            response.sendRedirect(SystemConstant.WEB_DOMAIN_NAME + "music" + SystemConstant.WEB_INDEX_HTML);
        } else {
            response.sendRedirect(SystemConstant.WEB_DOMAIN_NAME);
        }
    }
}
