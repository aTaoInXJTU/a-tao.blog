package com.atao.controller;

import com.atao.constants.SystemConstant;
import com.atao.domain.Result;
import com.atao.service.ArticleService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/content/article")
public class ArticleController {


    @Resource
    private ArticleService articleService;

    @GetMapping("/list")
    public Result articleList(
            @RequestParam(defaultValue = "0")Integer pageNum,
            @RequestParam(defaultValue = SystemConstant.DEFAULT_PAGE_SIZE_STR) Integer pageSize,
            Long categoryId) {
        return articleService.articleList(pageNum, pageSize, categoryId);
    }

    @GetMapping("/{id}")
    public Result articleGet(@PathVariable Long id) {
        return articleService.getArticleDetail(id);
    }

    /**
     * 获取前一篇和后一篇
     */
    @GetMapping
    public Result getBeforeAfterArticles(@RequestParam("id") Long id) {
        return articleService.getBeforeAfterArticles(id);
    }

    /**
     * 更新文章反应
     */
    @PostMapping("/reaction")
    public Result updateReaction(@RequestBody Map<String, Object> map) {
        Long articleId = (long) ((Integer) map.get("articleId"));
        List<Integer> cnt = (List<Integer>) map.get("cnt");
        ArrayList<Long> list = new ArrayList<>();
        for (Integer integer : cnt) {
            list.add(integer.longValue());
        }
        return articleService.updateReaction(articleId, list);
    }

    /**
     * 评论对应文章
     */
    @GetMapping("/categoryArticle")
    public Result getCategoryArticle() {
        return articleService.getCategoryArticle();
    }

    /**
     * 标签及标签对应文章数量
     */
    @GetMapping("/tagArticle")
    public Result getTagArticle() {
        return articleService.getTagArticle();
    }

    /**
     * 标签对应文章
     */
    @GetMapping("/listByTagId/{id}")
    public Result listByTagId(@PathVariable("id") Long id) {
        return articleService.listByTagId(id);
    }

    /**
     * 目录对应文章
     */
    @GetMapping("/listByCategoryId/{id}")
    public Result listByCategoryId(@PathVariable("id") Long id) {
        return articleService.listByCategoryId(id);
    }
}
