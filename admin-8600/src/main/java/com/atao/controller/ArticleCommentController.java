package com.atao.controller;

import com.atao.domain.Result;
import com.atao.domain.entity.ArticleComment;
import com.atao.service.ArticleCommentService;
import com.atao.service.AuthorService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/content/articleComment")
public class ArticleCommentController {
    @Resource
    private ArticleCommentService articleCommentService;

    @Resource
    private AuthorService authorService;

    @GetMapping("/{id}")
    public Result getArticleCommentByArticleId(@PathVariable Long id) {
        return articleCommentService.getArticleCommentByArticleId(id);
    }

    @HystrixCommand(fallbackMethod = "errUpdateReaction",
            commandProperties =
                    {
                            //规定 1 秒钟以内就不报错，正常运行，超过 5 秒就报错，调用指定的方法
                        @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1000"),
                            //只能有20个并发线程来访问这个方法
                        @HystrixProperty(name="execution.isolation.strategy", value="SEMAPHORE"),
                        @HystrixProperty(name="execution.isolation.semaphore.maxConcurrentRequests", value="20"),
                            //是否开启熔断器
                        @HystrixProperty(name = "circuitBreaker.enabled", value = "true"),
                            //统计时间窗
                        @HystrixProperty(name = "metrics.rollingStats.timeInMilliseconds",value = "1000"),
                            //统计时间窗内请求次数
                        @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "20"),
                            //休眠时间窗口期
                        @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
                            //在统计时间窗口期以内，请求失败率达到 60% 时进入熔断状态
                        @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "60"),
                    })
    @PostMapping("/updateReaction")
    public Result updateReaction(@RequestBody Map<String, Object> map) {
        Integer id = (Integer) map.get("articleId");
        Integer commentId = (Integer) map.get("commentId");
        Integer like = (Integer) map.get("like");
        List<Integer> cnt = (List<Integer>) map.get("cnt");
        return articleCommentService.updateReaction(id, commentId, like, cnt);
    }

    //服务限流callback
    public Result errUpdateReaction(@RequestBody Map<String, Object> map) {
        return Result.fail("请求次数过多或超时！");
    }

    @PostMapping
    public Result addArticleComment(@RequestBody ArticleComment articleComment) {
        return articleCommentService.addArticleComment(articleComment);
    }

    @GetMapping("/{info}/static_str")
    public Result getArticleCommentAuthor(@PathVariable String info) {
        return authorService.getCurAuthor(info);
    }
}
