package com.atao.commentLine;

import com.atao.service.ArticleService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * springboot 启动时执行自定义方法
 */
@Component
@Order(2)
class MyCommandLineRunner implements CommandLineRunner {
    @Resource
    private ArticleService articleService;

    @Override
    public void run(String... args) throws Exception {
        //启动时更新local-search.xml
        articleService.updateLocalSearchXML();
    }
}
